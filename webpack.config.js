const path = require('path');
const autoprefixer = require('autoprefixer');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const Dotenv = require('dotenv-webpack');

module.exports = env => {
	
	const envPath = env.ENVIRONMENT ? `.env.${env.ENVIRONMENT}` : '.env';
	console.debug("envPath=" + envPath);
	
	return {
    entry: './src/pages/Main.js',
    output: {
        path: path.resolve(__dirname, 'dist/bankingweb'),
        filename: 'app.js',
        chunkFilename: '[id].js',
        publicPath: 'bankingweb'
    },
    resolve: {
        extensions: ['.js', '.jsx']
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            },
            {
                test: /\.css$/,
                exclude: /node_modules/,
                use: [
                    { loader: 'style-loader' },
                    { 
                        loader: 'css-loader',
                        options: {
                            modules: {
                                localIdentName: "[name]__[local]___[hash:base64:5]",
                            },														
                            sourceMap: true
                        }
                     },
                     { 
                         loader: 'postcss-loader',
                         options: {
                             ident: 'postcss',
                             plugins: () => [
                                 autoprefixer({})
                             ]
                         }
                      }
                ]
            },
            {
                test: /\.(png|jpe?g|gif)$/i,
                use: [
                    {
                      loader: 'url-loader',
                      options: {
                    	name: '[path][name].[ext]',
                        outputPath: 'images',
                      }  
                    }
                  ]
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: __dirname + '/src/index.html',
            filename: 'index.html',
            inject: 'body'
        }),
        new CopyPlugin({
        	patterns: [
            {from: 'src/images', to: 'images'},
            {from: 'src/css', to: 'css'},
            {from: 'src/locales', to: 'locales'},
            {from: 'src/resources', to: 'resources'}
            ]
        }),
        new Dotenv({
            path: envPath
        })
    ]
	};
};