import Network from './Network'

const ENDPOINT_PATH = Object.freeze({
	LOGIN : '/banking/rest/auth/login',
	LOGOUT : '/banking/rest/auth/logout',
	MFA : '/banking/rest/auth/mfa/answer',
	ACCOUNT_SUMMARY : '/banking/rest/account/summary',
	ACCOUNT_DETAILS : '/banking/rest/account/details'
});

class RestProxy {
	constructor() {
		this.network = new Network();
	}
	
	sendRequest = (urlKey, inputParams, onSuccess, onFailure, onNetworkFailure) => {
		var url = ENDPOINT_PATH[urlKey]
		var body = JSON.stringify(inputParams);
		this.network.post(url, body, onSuccess, onFailure, onNetworkFailure)
	}
}

export default RestProxy