import i18next from 'i18next';
import XHR from 'i18next-xhr-backend'

export default class I18n {
	
	//init should be called when the application is loaded
	init = (lng, context_root) => {
		  
		i18next
		  .use(XHR)
		  .init({
		    backend: {
				loadPath: context_root + '/locales/{{lng}}/{{ns}}.json'
		    },
		    lng: lng,
		    fallbackLng: 'en',
		    detectLngFromHeaders: true
		  })
	}
	
	changeLanguage = (lng, updateContent) => {
		BankingBase.language = lng
		i18next.changeLanguage(lng, function(err, t) {updateContent(t)});
	}
	
	t = (key) => {
		return i18next.t(key)
	}
}