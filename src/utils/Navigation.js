const PAGE_PATH = Object.freeze({
	LOGIN : '/',
	LOGOUT : '/',
	START : '/',
	MFA_ANSWER : '/mfa',
	ACCOUNT_SUMMARY : '/accounts',
	ACCOUNT_DETAILS : '/accounts/details'
});

class Navigation {
	
	navigateTo(path, fromComponent, urlMatchingKey, params) {
		var path = urlMatchingKey? PAGE_PATH[path] + "/" + urlMatchingKey : PAGE_PATH[path]
		fromComponent.props.history.push({pathname:path, state:params})
	}
	
	isNavigatable(path) {
		return PAGE_PATH[path];
	}
	
	openURL(url) {
		window.open(url, '_blank');
	}
}

export default Navigation