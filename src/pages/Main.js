import { HashRouter as Router, Route, Switch } from 'react-router-dom';
import React from 'react';
import ReactDOM from 'react-dom';
import Login from './auth/login/Login';
import Logout from './auth/logout/Logout';
import Mfa from './auth/mfa/Mfa';
import Accounts from './accounts/Accounts';
import AccountDetails from './accounts/AccountDetails';
import UnderConstruction from '../components/UnderConstruction';
import BankingBase from '../base/BankingBase';
import LoadingBar from '../components/loading-bar/LoadingBar'

window.BankingBase = new BankingBase();

ReactDOM.render(
	<Router basename={window.BankingBase.CONTEXT_ROOT}>
		<div class="container variableHeight height100">
			<LoadingBar/>
			<Switch>
			    <Route exact path="/" component={Login}/>
			    <Route path="/logout" component={Logout}/>
			    <Route exact path="/mfa" component={Mfa}/>
			    <Route exact path="/accounts" component={Accounts}/>
			    <Route path="/accounts/details/:accountKey" component={AccountDetails}/>
			    <Route path="/settings" component={UnderConstruction}/>
			    <Route path="/move-money" component={UnderConstruction}/>
			    <Route path="/locator" component={UnderConstruction}/>
			    <Route path="/contact-us" component={UnderConstruction}/>
			    <Route path="/security" component={UnderConstruction}/>
			    <Route path="/user-management/activate" component={UnderConstruction}/>
			    <Route path="/user-management/recover" component={UnderConstruction}/>
		    </Switch>
		</div>
	</Router>,
	document.querySelector("#page")
)