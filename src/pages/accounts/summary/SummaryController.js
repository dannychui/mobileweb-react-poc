import EventHandler from '../../../base/EventHandler'
import AccountSummaryViewModel from './AccountSummaryViewModel'

export default class SummaryController extends EventHandler {

	constructor(summaryMain) {
		super();
		this.summaryMain = summaryMain;
		this.accountSummaryViewModel = new AccountSummaryViewModel()
	}
	
	loadAccounts = () => {
		this.executeSubmit('ACCOUNT_SUMMARY');
	}
	
	onSubmitSuccess = (result) => {
		console.log("Accounts load success.")

		this.accountSummaryViewModel.setResponseAccountsAndController(result.response.accounts, this);
		var accountSummarySections = this.accountSummaryViewModel.accountSummarySections;
		
		//keep track of the currently selected account view.
		this.currentAccountView = this.accountSummaryViewModel.getInitialAccountView();
		
		//update view
		this.summaryMain.setState({accountSummarySections : accountSummarySections});
		this.summaryMain.accountsMain.select(this.currentAccountView.key, this.isWideView(), true);
		
		//force resize event to initialize certain states.
		this.handleResize();
	}
	
	select = (view) => {
		this.currentAccountView.deselect();
		this.currentAccountView = view;
		this.summaryMain.setState(this.summaryMain.state);
		this.summaryMain.accountsMain.select(this.currentAccountView.key, this.isWideView(), false);
	}
	
	getOriginatingPage = () => {
		return this.summaryMain.accountsMain;
	}
}