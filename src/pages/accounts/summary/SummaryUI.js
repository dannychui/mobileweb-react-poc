import React from 'react'

const SummaryUI = (props) =>
	
		<div class="account-summary component-container variableHeight">
			<div class="title">
				<span class="medium red title" tabIndex="0">{props.viewModel.AccountSummary}</span>
			</div>
			<div id="accountSummarySections">
				{props.viewModel.accountSummarySections.map((section, index, array) => <SectionUI section={section} key={index} array={array}/>)}
			</div>
		</div>


const SectionUI = (props) =>
	<div>
		<div class="header-1">
			<span class="header-1-type small dark-grey" tabIndex="0">{props.section.category}</span>
		</div>
		<div id="records">
			{props.section.records.map((record, index, array) => <RecordUI record={record} key={index} array={array}/>)}
		</div>
	</div>
	
const RecordUI = (props) => 
	<div tabIndex="0" className={props.record.view.className} onClick={props.record.view.select}>
		<div class="account-1-top">
			<span class="account-1-top-left small dark-grey">{props.record.model.description}</span>
			<span class="account-1-top-right small dark-grey">{props.record.model.accountNum}</span>
		</div>
		<div id="balances">
			{props.record.model.balances.map((balance, index, array) => <BalanceUI balance={balance} key={index} array={array}/>)}
		</div>
	</div>

const BalanceUI = (props) =>
	<div class="account-1-bottom">
		<span class="account-1-bottom-right extra-large dark-grey" key={props.index}>{props.balance.amount}</span>
		<span class="account-1-bottom-currency dark-grey small">{props.balance.currency}</span>
	</div>

export default SummaryUI