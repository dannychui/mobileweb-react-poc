import React from 'react'
import SummaryController from './SummaryController'
import SummaryUI from './SummaryUI'
import PageBase from '../../../base/PageBase'

export default class Summary extends React.Component {
	constructor(props) {
		super(props);
		this.accountsMain = props.accountsMain;
		this.controller = new SummaryController(this);
		this.viewModel = this.controller.accountSummaryViewModel;
		this.pageBase = new PageBase(this)
	}

	componentDidMount() {
		this.controller.loadAccounts();
	}
	
	render () {
		return (
			<SummaryUI viewModel={this.viewModel}/>
		);
	}
}