import React from 'react'
import NavBar from '../../components/nav-bar/NavBar'
import Details from './details/Details'
import NotificationBar from '../../components/notification-bar/NotificationBar'

class AccountDetails extends React.Component {
	constructor(props) {
		super(props)
		this.match = props.match;
	}
	render() {
		return (
			<div class="container variableHeight f-topbar-fixed">
				<div>
					<NavBar/>
				</div>
				<div>
					<NotificationBar/>
				</div>
				<div class="row variableHeight">
					<Details accountsMain={this} accountKey={this.match.params.accountKey} accountDetailsOnly="true"/>
				</div>
			</div>				
		);
	}
}

export default AccountDetails