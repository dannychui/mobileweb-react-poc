import React from 'react'

const DetailsUI = (props) =>

	<div id="details" class="account-details component-container variableHeight">
		<div class="account-2-container">
			<div class="account-2">
				<div class="account-2-top">
					<span class="account-1-top-center small" tabIndex="0">{props.viewModel.accountName}</span>
					<span class="account-2-top-center small" tabIndex="0">{props.viewModel.accountNum}</span>
				</div>
				<div class="account-summary-amount">
		            <span class="account-2-center extra-large black" tabIndex="0">{props.viewModel.balanceCurrency}</span>
		        </div>
		    </div>
		    <div>
			    <ul class="tabs">
			        <li class="tab-title two-tabs active" tabIndex="0"><a href="javascript:;" id="tabActivity" tabIndex="-1" onClick={props.controller.panelActivity}><span class="small">{props.viewModel.Transactions}</span></a></li>
			        <li class="tab-title two-tabs" tabIndex="0"><a href="javascript:;" id="tabDetails" onClick={props.controller.panelDetails}><span tabIndex="-1" class="small">{props.viewModel.AccountDetails}</span></a></li>
			    </ul>
			</div>
		</div>
	
		<div class="tabs-container">
		    <div class="tabs-content">
		        <div class="content active" id="panelActivity">
		        		<ul class="pricing-table">
		        			{props.viewModel.historyGrouping.map((section, index, array) => <HistoryGroupingUI section={section} key={index} array={array}/>)}
		        		</ul>
					<div id="history24section" class="header-1">
						<span class="header-2-type small" tabIndex="0">
						{props.viewModel.History24}
						</span>
						<span class="header-2-type small" tabIndex="0">
					  		<a href="https://www.google.com" target="_blank" id="twentyFourMonth" class="clickable input-action small blue">
					  				{props.viewModel.Site}
					  		</a>
					  	</span>
					  	<span class="header-2-type small" tabIndex="0">
					  		{props.viewModel.Disclaimer}
					  	</span>
					</div>
		        </div>
		        	<div class="content" id="panelDetails">
		        		<ul class="pricing-table">
	        				{props.viewModel.detailListGroup.map((section, index, array) => <DetailsListUI section={section} key={index} array={array}/>)}
	        			</ul>
		        	</div>
		    </div>
		</div>
	</div>


const HistoryGroupingUI = (props) =>
<div>
	
		<div class="bullet-item date summary-item clearfix" tabIndex="0">
	    		<span class="left list-2-left">{props.section.dategroup}</span>
	    </div>
	    <div id="records">
			{props.section.records.map((record, index, array) => <HistoryRecordUI record={record} key={index} array={array}/>)}
		</div>

</div>

const HistoryRecordUI = (props) =>
<div>
	<div class="bullet-item summary-item clearfix">
		<div class="small-6 medium-6 large-6 columns">
			<div class="sub-text black transaction-description" tabIndex="0">{props.record.description}</div>
		</div>
		<div class="small-6 medium-6 large-6 columns">
			<div class="right list-2-right" tabIndex="0">{props.record.amount}</div>
		</div>
	</div>
</div>

const DetailsListUI = (props) =>

		<div class="row">

	        <div class="bullet-item details-item clearfix">
	           
	            <div class="sub-text black list-2-right" tabIndex="0" data-bind="text: detail"></div>
	           
	           
	            <div class="sub-text black list-2-right" tabIndex="0" data-bind="text: detail"></div>
	           
	            
	            <div class="small-6 medium-6 large-6 columns">

	                <div class="sub-text black" tabIndex="0" data-bind="text: description">{props.section.description}</div>

	            </div>
	            <div class="small-6 medium-6 large-6 columns">
	                <div class="right list-2-right" tabIndex="0" data-bind="text: detail">{props.section.detail}</div>
	            </div>

	        </div>
	    </div>


export default DetailsUI