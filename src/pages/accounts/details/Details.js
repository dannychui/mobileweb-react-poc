import React from 'react'
import DetailsController from './DetailsController'
import DetailsUI from './DetailsUI'
import AccountDetailsViewModel from './AccountDetailsViewModel'
import PageBase from '../../../base/PageBase'

export default class Details extends React.Component {
	constructor(props) {
		super(props);
		this.accountsMain = props.accountsMain;
		this.controller = new DetailsController(this);
		//when navigating to /accounts/details in a small device, accountKey is passed in in the url
		this.accountKey = props.accountKey;
		this.viewModel = this.controller.accountDetailsViewModel;;
		this.pageBase = new PageBase(this)
	}
	
	loadAccount(accountKey) {
	  	this.controller.loadAccount(this.accountKey);
	}
	 
	componentDidMount = () => {
		if (this.accountKey) {
			this.controller.loadAccount(this.accountKey);
		}
	}
	
	render () {
		return (
			<DetailsUI viewModel={this.viewModel} controller={this.controller}/>
		);
	}
}