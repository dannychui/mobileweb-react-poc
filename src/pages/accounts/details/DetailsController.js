import AccountDetailsViewModel from './AccountDetailsViewModel'
import EventHandler from '../../../base/EventHandler'

class DetailsController extends EventHandler {

	constructor(detailsMain) {
		super(detailsMain);
		this.detailsMain = detailsMain;
		this.accountDetailsViewModel = new AccountDetailsViewModel();
	}
	
	loadAccount = (accountKey) => {
	    this.executeSubmit('ACCOUNT_DETAILS', {accountKey:accountKey});
	}
	
	onSubmitSuccess = (result) => {
		console.log("Account details load success."+result.response.account.accountKey);
		this.accountDetailsViewModel.setAccount(result.response.account);
		this.detailsMain.setState({account : this.accountDetailsViewModel});
		
		if (this.detailsMain.accountKey) {
			//details only view. need to force handleResize
			this.handleResize();
		}
	}
	
	panelDetails = () => {
		this.changeTab('tabDetails','panelDetails', this.detailsMain)
	}
	
	panelActivity = () => {
		this.changeTab('tabActivity', 'panelActivity', this.detailsMain)
	}
	
	getOriginatingPage = () => {
		return this.detailsMain.accountsMain;
	}
}

export default DetailsController