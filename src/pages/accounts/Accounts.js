import React from 'react'
import Summary from './summary/Summary'
import Details from './details/Details'
import NavBar from '../../components/nav-bar/NavBar'
import NotificationBar from '../../components/notification-bar/NotificationBar'

class Accounts extends React.Component {
	constructor(props) {
		super(props);
		this.select = this.select.bind(this);
	}

	select(accountKey, isWideView, initialLoading) {
		if (isWideView || initialLoading) {
			this.refs.detailsMain.loadAccount(accountKey);
		} else {
			BankingBase.navigation.navigateTo("ACCOUNT_DETAILS", this, accountKey)
		}
	}
	
	render () {
		return (
			<div class="container variableHeight f-topbar-fixed">
				<div>
					<NavBar/>
				</div>
                <div>
                		<NotificationBar/>
                </div>
				<div class="row variableHeight">
					<div class="medium-5 columns no-padding variableHeight">
						<Summary accountsMain={this}/>
					</div>
					<div class="medium-7 columns variableHeight detailsVisibility">
						<Details accountsMain={this} ref="detailsMain"/>
					</div>
				</div>
			</div>
		);
	}
}

export default Accounts
