import React from 'react'
import TitleBar from '../../../components/title-bar/TitleBar'
import NotificationBar from '../../../components/notification-bar/NotificationBar'

const MfaUI = (props) =>

<div class="container variableHeight f-topbar-fixed">
	<div>
		<TitleBar/>
	</div>
	<div>
		<NotificationBar/>
	</div>
	<div class="mfa-container component-container" id="mfa">
	    <div class="margin">
	        <div class="title">
	            <span class="medium red title" data-bind="text: lbl.MFASecurityQuestion">{props.viewModel.MFASecurityQuestion}</span>
	        </div>
	        <form id="mfaForm" data-abide method="post">
	            <div id="question" class="clearfix mb7">
	                <span id="questionInputText" class="small dark-grey left" for="answerInputText">{props.viewModel.question}</span>
	            </div>
	            <div>
	                <input type="text" placeholder={props.viewModel.MFAAnswer} id="answerInputText" class="answer" autoComplete="off" required onChange={props.controller.handleAnswerChange}/>
	                <small class="error">{props.viewModel.PleaseSelectAQuestion}</small>
	            </div>
	                <div>
	                    <button id="continue" class="red-button mt10" onClick={props.controller.handleSubmit} >{props.viewModel.Continue}</button>
	                </div>
	            <div id="disclaimer" class="clearfix">
	                <span class="small dark-grey left">{props.viewModel.MFADisclaimer}</span>
	            </div>
	        </form>
	    </div>
	</div>
</div>

export default MfaUI