import ViewModelBase from '../../../base/ViewModelBase'

export default class MfaViewModel extends ViewModelBase {
	constructor(question) {
		super()
		this.question = question;
		this.answer = '';
		
		this.fieldMap = {
			MFASecurityQuestion: "MFA.SecurityQuestion",
			PleaseSelectAQuestion: "Validation.PleaseSelectAQuestion",
            MFAAnswer :"MFA.Answer",
            MFADisclaimer :"MFA.Disclaimer",
            Continue:"Continue"
		}
	}
}