import React from 'react'
import MfaController from './MfaController'
import MfaViewModel from './MfaViewModel'
import MfaUI from './MfaUI'
import PageBase from '../../../base/PageBase'

export default class Mfa extends React.Component {
	constructor(props) {
		super(props);
		this.controller = new MfaController(this, props.location.state);
		this.viewModel = this.controller.mfaViewModel
		this.pageBase = new PageBase(this)
	}

	render () {
		return <MfaUI notificationHandler={this}
				controller={this.controller}
				viewModel={this.viewModel}/>;
	}
}