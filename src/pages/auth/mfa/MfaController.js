import MfaViewModel from './MfaViewModel'
import EventHandler from '../../../base/EventHandler'

export default class MfaController extends EventHandler {

	constructor(mfa, params) {
		super(mfa);
		this.mfa= mfa;
		this.mfaViewModel = new MfaViewModel(params.question);
	}
	
	handleAnswerChange = (event) => {
		this.mfaViewModel.answer = event.target.value;
		console.log("this.mfaViewModel.answer=" + this.mfaViewModel.answer);
	}
	
	handleSubmit = (event) => {
	    var params = {question:this.mfaViewModel.question, answer:this.mfaViewModel.answer}
	    this.executeSubmit('MFA', params, event)
	}
	
	onLoadSuccess = (result) => {
		console.log("MFA load success.")
		
		//trigger resize event to make sure account page displayed properly
		this.handleResize();
	}
	
	onSubmitSuccess = () => {
		console.log("MFA success.")
		BankingBase.navigation.navigateTo("ACCOUNT_SUMMARY", this.mfa)
	}
	
	getOriginatingPage = () => {
		return this.mfa;
	}
}