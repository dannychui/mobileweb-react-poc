import EventHandler from '../../../base/EventHandler'
import LoginViewModel from './LoginViewModel'

export default class LoginController extends EventHandler {

	constructor(login) {
		super(login)
		this.login= login;
		this.loginViewModel = new LoginViewModel();
	}
	
	handleUserIdChange = (event) => {
		this.loginViewModel.userId = event.target.value;
		console.log("this.loginViewModel.userId=" + this.loginViewModel.userId);
	}
	handlePasswordChange = (event) => {
		this.loginViewModel.password = event.target.value;
		console.log("this.loginViewModel.password=" + this.loginViewModel.password);
	}
	
	handleSubmit = (event) => {
	    var params = {userId:this.loginViewModel.userId, password:this.loginViewModel.password}
	    this.executeSubmit('LOGIN', params, event);
	}
	
	onSubmitSuccess = (result) => {
		console.log("Login success."+result)
		BankingBase.navigation.navigateTo("ACCOUNT_SUMMARY", this.login)
	}
	
	getOriginatingPage = () => {
		return this.login;
	}
	
	toggleLanguage = () => {
		var langBtn = Array.from(document.getElementsByClassName("lang"));
		
		Array.from(langBtn).map((btn) => {
			if (!btn.classList.contains("checked-lang")) {
				btn.classList.add("checked-lang")
			} else {
				btn.classList.remove("checked-lang")
			}
	    })
	    
	    this.loginViewModel.language = (this.loginViewModel.language == "fr")? "en" : "fr";
		//BankingBase.language = this.loginViewModel.language;
		console.log("this.loginViewModel.language=" + this.loginViewModel.language);
		
		//this.login.setState(this.loginViewModel);
		this.login.pageBase.changeLanguage(this.loginViewModel.language)
	}
	
	handleLocator = (event) => {
		BankingBase.navigation.openURL(this.loginViewModel.locatorURL)
	}
	
	handleSecurity = (event) => {
		BankingBase.navigation.openURL(this.loginViewModel.securityAndPrivacyURL)
	}
}
