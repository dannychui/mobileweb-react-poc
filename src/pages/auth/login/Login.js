import React from 'react'
import LoginController from './LoginController'
import LoginUI from './LoginUI'
import NotificationBar from '../../../components/notification-bar/NotificationBar'
import PageBase from '../../../base/PageBase'

class Login extends React.Component {
	constructor(props) {
		super(props)
		this.controller = new LoginController(this)
		this.viewModel = this.controller.loginViewModel
		this.pageBase = new PageBase(this)
	}

	render () {
		return (
			<div>
			<NotificationBar/>
			<LoginUI
				controller={this.controller}
			    viewModel={this.viewModel}/>
			</div>
		);
	}
}

export default Login
