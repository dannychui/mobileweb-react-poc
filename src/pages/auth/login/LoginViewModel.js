import ViewModelBase from '../../../base/ViewModelBase'

export default class LoginViewModel extends ViewModelBase {
	
	constructor(props) {
		super(props)
		this.userId = '';
		this.password = '';
		
		this.fieldMap = {
			LogoSVG: "Images.login-logoSVG",
			CardNumber: "Login.CardNumber",
			Password: 'Login.Password',
            SaveToggle: 'Login.SaveToggle',
            ResetPassword: 'Login.ResetPassword',
            SignIn: 'Login.SignIn',
            ContactUs: 'ContactUs',
            Locator: 'Login.Locator',
            Security: 'Login.Security',
            Activate: 'Login.Activate',
            LocatorURL: 'URLs.locator',
			securityAndPrivacyURL: 'URLs.securityAndPrivacy'
		}
	}
}