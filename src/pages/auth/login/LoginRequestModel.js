class LoginRequestModel {
	this.languageCode = "en";
	this.password = {};
	this.saveToken = false;
	this.userId = {};
	this.userKey = {};
	this.paymentReference = {};
	this.paymentType = {};
}

export default LoginRequestModel