import React from 'react'
import {Link} from 'react-router-dom'

const LoginUI = (props) =>
	<div class="component-container">
		<div>
			<img id="loginLogo" class="login-logo" src={props.viewModel.LogoSVG}></img>
		</div>
		
		<form id="loginForm" method="post" autoComplete="off">
			<div class="margin">
				<div class="clearfix">
					<label id="cardNumberInputLabel" class="login small dark-grey left" for="cardNumberInputText">{props.viewModel.CardNumber}</label>
					<label id="saveCheckLabel" class="small dark-grey right" for="saveCheck">{props.viewModel.SaveToggle}</label>
				</div>
				<div class="input-1">
					<div>
						<input id="cardNumberInputText" type="text" class="login-card-number small" autoComplete="off" onChange={props.controller.handleUserIdChange}/>
					</div>
					<div class="save-card">
						<input id="saveCheck" type="checkbox" class="save-card" name="saveCheck" data-bind="checked: cardSaved"/>
						<label tabIndex="0" id="saveCheckLabelStar" class="save-card" for="saveCheck"></label>
					</div>
				</div>
				<label id="passwordInputLabel" class="small dark-grey" for="passwordInputText">{props.viewModel.Password}</label>
				<div>
					<input id="passwordInputText" type="password" class="password" autoComplete="off" onChange={props.controller.handlePasswordChange}/>
				</div>
			</div>
			<div class="row login" id="signin">
				<div class="large-12 columns">
				<button type="submit" id="signOnButton" class="red-button mb20" onClick={props.controller.handleSubmit}>{props.viewModel.SignIn}</button>
				</div>
				<div class="large-12 columns c">
					<Link className="clickable naviLinks login-footer-links extra-small black menu-contact" to="/user-management/recover">{props.viewModel.ResetPassword}</Link>
				</div>
			</div>
		</form>
		
		<div class="block-icons">
			<ul class="small-block-grid-1" >
				<li>
					<Link className="clickable naviLinks login-footer-links extra-small black menu-contact" to="/user-management/activate">{props.viewModel.Activate}</Link>
				</li>
				<li>
					<Link className="clickable naviLinks login-footer-links extra-small black menu-locator" to="/locator">{props.viewModel.Locator}</Link>
				</li>
				<li>
					<Link className="clickable naviLinks login-footer-links extra-small black menu-contact" to="/contact-us">{props.viewModel.ContactUs}</Link>
				</li>
				<li>
					<Link className="clickable naviLinks login-footer-links extra-small black menu-security-privacy" to="/security">{props.viewModel.Security}</Link>
				</li>
			</ul>
		</div>
		
		<div id="language" class="radio-toolbar">
			<button class="lang checked-lang" onClick={props.controller.toggleLanguage} aria-label="English">EN</button>
			<button class="lang" onClick={props.controller.toggleLanguage} aria-label="Francais">FR</button>
		</div>
	</div>

export default LoginUI