import React from 'react'
import EventHandler from '../../../base/EventHandler'

class Logout extends React.Component {
	constructor(props) {
		super(props);
		this.controller = new LogoutController(this);
	}

	componentDidMount = () => {
		this.controller.handleLogout();
	}
	
	render () {
		return null
	}
}

class LogoutController extends EventHandler {
	
	constructor(logout) {
		super(logout)
		this.logout= logout;
	}
	
	handleLogout = (params) => {
		this.executeSubmit('LOGOUT');
	}
	
	getOriginatingPage = () => {
		return this.logout;
	}
}

export default Logout
