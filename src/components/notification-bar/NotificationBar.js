import React from 'react'

class NotificationBar extends React.Component {

	constructor(props) {
		super(props);
        this.state = {notification:{classNames:"hidden-notification alert-box", text:""}}
        BankingBase.notificationBar = this;
	}
	
	render () {
		return (
			<NotificationBarUI notification={this.state.notification}/>
		);
	}
	
	handleError = (message) => {
		this.setState({notification:{classNames:"error-message alert-box", text:message}})
	}
	
	handleNetworkError = (data) => {
		this.setState({notification:{classNames:"default-message alert-box", text:"We are experiencing technical difficulty. Please try again later."}})
	}
	
	handleConfirmation = (message) => {
		this.setState({notification:{classNames:"confirmation-message alert-box", text:message}})
	}
}

const NotificationBarUI = (props) => 
<div class="notifications" aria-live="assertive" >
	<div class={props.notification.classNames} role="alertdialog" tabindex="-1">
		<span class="offscreen" alt="Notification"></span>
		<span class="notification-text" tabindex="0">{props.notification.text}</span>
	</div>
</div>

export default NotificationBar