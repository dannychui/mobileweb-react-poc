import React from 'react'

export default class LoadingBar extends React.Component {
	constructor(props) {
		super(props);
		this.favicon = BankingBase.imagesPath + "favicon.ico";
        this.state = {visiblility: "hidden", favicon: this.favicon}
        //add a ref to the LoadingBar in the page component so the LoadingBar can be manipulated later
        BankingBase.loadingBar = this;
	}
	
	start = () => {
		this.setState({visiblility: "visible"})
	}
	
	stop = () => {
		this.setState({visiblility: "hidden"})
	}
	
	render () {
		return (
			<LoadingBarUI visiblility={this.state.visiblility} favicon={this.state.favicon}/>
		);
	}
}

const LoadingBarUI = (props) => 
<div class="loading-container" role="alert" aria-live="assertive" tabindex="0" style={{ visibility: props.visiblility }}>
	<div id="load" aria-label="Loading"><div style={{color: "transparent"}}><div class="loader"></div></div></div>
</div>