import React from 'react'
import {Link} from 'react-router-dom'

const NavBarUI = (props) => 
	<div>
	    <div class="nav-bar fixed">
	        <nav class="top-bar" role="navigation">
	            <ul class="title-area">
	                
	                <li class="toggle-topbar">
	                    <img class="menu"  role="button" alt="Menu" tabIndex="0" src={props.viewModel.menuIcon} onClick={props.viewModel.toggleMenu}></img>
	                </li>
	            </ul>
                <section class="middle tab-bar-section">
                		<img class="logo" tabIndex="0" alt="GlobeBank" src={props.viewModel.logo}></img>
                	</section>
	            <section class="top-bar-section">
	                <NavigationMenuUI menuItems={props.viewModel.menuItems}/>
	            </section>
	        </nav>
	    </div>
	</div>

const NavigationMenuUI = (props) =>
	<div class="navigation-menu">
	  	<ul class="right">
	  		{props.menuItems.map((menuItem, index) => <MenuItemUI menuItem={menuItem} key={index}/>)}
	  	</ul>
	</div>

const MenuItemUI = (props) => 
	<li>
		<Link className={props.menuItem.iconClass} to={props.menuItem.path} onClick={props.menuItem.handleClick}>{props.menuItem.description}</Link>
	</li>
	
export default NavBarUI