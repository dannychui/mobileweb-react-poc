import React from 'react'
import NavBarUI from './NavBarUI'
import NavBarViewModel from './NavBarViewModel'

export default class NavBar extends React.Component {

	constructor(props) {
		super(props);
		this.viewModel = new NavBarViewModel();
	}
	
	render () {
		return (
			<NavBarUI viewModel={this.viewModel}/>
		);
	}
}
