import React from 'react';
import {Link} from 'react-router-dom'

const UnderConstruction = (props) => 

	<div class="row">
		<div class="small-3 small-centered columns">
			<div>
				<img src={BankingBase.imagesPath + "under-construction.jpeg"}></img>
			</div>
			<div>
				<button type="submit" id="goBackButton" class="red-button mt20" ><a href="javascript:history.go(-1)" style={{color: "white"}}>Go Back</a></button>
			</div>
		</div>
	</div>

export default UnderConstruction