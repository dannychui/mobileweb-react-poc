import React from 'react'

export default class TitleBar extends React.Component {

	constructor(props) {
		super(props);
		this.logo = BankingBase.CONTEXT_ROOT + "/images/"+ BankingBase.language + "/globebank-logo.png";
		this.state = {logo : this.logo};
	}
	
	render () {
		return (
			<TitleBarUI logo={this.state.logo}/>
		);
	}
}

const TitleBarUI = (props) => 
<div>
    <div class="fixed">
        <nav class="top-bar">
            <section class="middle tab-bar-section">
            		<img class="title-bar logo" tabIndex="0" alt="GlobeBank" src={props.logo}></img>
            	</section>
        </nav>
    </div>
</div>