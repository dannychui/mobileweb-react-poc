import React from 'react'

//PageBase defines the common functionality for all components. It is needed since React 
//does not support Component inheritance
class PageBase {
	constructor(page) {
		this.page = page;
		this.changeLanguage(BankingBase.language)
	}
	
	updateContent = (t) => {
		//every child defines viewModel field.
		var viewModel = this.page.viewModel
		viewModel.updateI18n(t)
		this.page.setState(viewModel)
	}
	
	changeLanguage = (lng) => {
		BankingBase.i18n.changeLanguage(lng, this.updateContent)
	} 
}

export default PageBase
