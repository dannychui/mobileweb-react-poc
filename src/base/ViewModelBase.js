export default class ViewModelBase {
	
	constructor(props) {
		this.i18n = BankingBase.i18n;
	}
	
	//Dynamically create/updates view model properties with internationalized values.
	//Each child class must have a fieldMap object, which maps field names to i18n lookup keys.
	updateI18n = (t) => {
		let vm = this
		for (let field of Object.keys(vm.fieldMap)) {
			vm[field] = this.i18n.t(vm.fieldMap[field])
		}
	}
}