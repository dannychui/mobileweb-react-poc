import RestProxy from '../utils/RestProxy'
import Navigation from '../utils/Navigation'
import I18n from '../utils/I18n'

class BankingBase {
	constructor(props) {
		this.language = "en";
		this.menuIconDisplayed = false;
		this.restProxy = new RestProxy();
		this.navigation = new Navigation();
		
		this.CONTEXT_ROOT = window.location.pathname;
		if (this.CONTEXT_ROOT.endsWith("/")) {
			//if deployed to the root, should use "" instead of "/" as context root
			this.CONTEXT_ROOT = this.CONTEXT_ROOT.substring(0, this.CONTEXT_ROOT.length - 1);
		}
		
		this.imagesPath = this.CONTEXT_ROOT + "/images/"
		
		this.i18n = new I18n()
		this.i18n.init(this.language, this.CONTEXT_ROOT)
	}
}

export default BankingBase