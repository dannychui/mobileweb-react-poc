export default class EventHandler {
	
	constructor(props) {

		this.minSplitWidth = 40.063;
		this.fontSize = window.getComputedStyle(document.body).fontSize;
		this.accountDetailsOnly = false;
		this.getOriginatingPage = null;
		this.activeButton = null;
		window.onresize = this.handleResize;
	}
	
	executeSubmit = (fromPage, params, event) => {
		//double-click protection
		if (event) {
			event.preventDefault();
			if (event.target.disabled == true) {
				return;
			}
			event.target.disabled = true;
			this.activeButton = event.target;
		}
		BankingBase.loadingBar.start();
		BankingBase.restProxy.sendRequest(fromPage, params, this.onSuccess, this.onFailure, this.onNetworkFailure);
	}
	
    handleConfirmationNotification = (message) => {
        console.log(message)
        BankingBase.notificationBar.handleConfirmation(message)
    }
    
    handleErrorNotification = (message) => {
        console.log(message)
        BankingBase.notificationBar.handleError(message)
    }
    
    handleNetworkErrorNotification = (data) => {
        console.log(data)
        BankingBase.notificationBar.handleNetworkError(data)
    }
    
	onSuccess = (result) => {
		//enable button
		if (this.activeButton) {
			this.activeButton.disabled = false;
		}
		if (!result || !result.context || !result.context.next || result.context.next.length == 0) {
			//let the controller decide what to do
			this.onSubmitSuccess(result);
			BankingBase.loadingBar.stop();
			return;
		}
		
		var next = result.context.next[0]
		var nextAction = next.action;
		var params = next.params;
		
		if (BankingBase.navigation.isNavigatable(nextAction)) {
			BankingBase.navigation.navigateTo(nextAction, this.getOriginatingPage(), null, params);
			BankingBase.loadingBar.stop();
		} else {
			BankingBase.restProxy.sendRequest(nextAction, params, this.onSuccess, this.onFailure, this.onNetworkFailure);
		}
	}
	
	onFailure = (response) => {
		//enable button
		if (this.activeButton) {
			this.activeButton.disabled = false;
		}
		console.log("submit failed.")
		BankingBase.loadingBar.stop();
		this.handleErrorNotification(response.exception.message)
	}
	
	onNetworkFailure = (data) => {
		//enable button
		if (this.activeButton) {
			this.activeButton.disabled = false;
		}
		console.log("network problem.")
		BankingBase.loadingBar.stop();
		this.handleNetworkErrorNotification(data)
	}
	
	changeTab = (selectedTabId, contentId, component) => {
		var tablinks = document.getElementsByClassName("tab-title");
		Array.from(tablinks).map((tab) => {
	        tab.className = tab.className.replace(" active", "");
	    })
		var anchors = document.querySelectorAll(".tab-title > a");
		var i;
		for (i = 0; i < anchors.length; i++) {
			if (anchors[i].id == selectedTabId) {
				anchors[i].parentElement.className += " active";
				break;
			}
		}
		
		var tabcontent = document.getElementsByClassName("tabs-content")[0];
		var content = tabcontent.getElementsByClassName("content")
	    Array.from(content).map((tab) => {
	    	tab.className = tab.className.replace(" active", "");
	    })
	    
	    var tabs = document.getElementById(contentId).className += " active";
		component.setState(component.state);
	}
	
	handleResize = () => {
		var variableHeight = Array.from(document.getElementsByClassName("variableHeight"));
		var detailsVisibility = Array.from(document.getElementsByClassName("detailsVisibility"));
		var toggleTopbar = document.getElementsByClassName("toggle-topbar")[0];
		BankingBase.menuIconDisplayed = window.getComputedStyle(toggleTopbar,null).getPropertyValue("display") != "none";
		
		if (this.isWideView()) {
			variableHeight.map((element, index) => {
				if (!element.classList.contains("height100")) {
					element.classList.add("height100")
				}
			})
			
			detailsVisibility.map((element, index) => {
				if (element.classList.contains("hiddenDisplay")) {
					element.classList.remove("hiddenDisplay")
				}
			})
		} else if (this.accountDetailsOnly == "true") {
			variableHeight.map((element, index) => {
				element.classList.remove("height100")
			})
			
			detailsVisibility.map((element, index) => {
				if (element.classList.contains("hiddenDisplay")) {
					element.classList.remove("hiddenDisplay")
				}
			})
		} else {
			variableHeight.map((element, index) => {
				element.classList.remove("height100")
			})
			
			detailsVisibility.map((element, index) => {
				if (!element.classList.contains("hiddenDisplay")) {
					element.classList.add("hiddenDisplay")
				}
			})
		}
	}
	
	windowWidth = () => {
		return document.documentElement.clientWidth
	}
	
	windowHeight = () => {
		return document.documentElement.clientHeight
	}
	
	windowsize = () => {
		return this.windowWidth() / parseFloat(this.fontSize)
	}
	
	isWideView = () => {
		return this.windowsize() > this.minSplitWidth
	}
}